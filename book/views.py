from django.shortcuts import render
from rest_framework import status
from rest_framework import generics, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from django_filters.rest_framework import DjangoFilterBackend
from .models import Author, Department, Book, Reader, BookOnHand
from .serializers import (
    AuthorSerializer, 
    DepartmentSerializer, 
    BookSerializer, 
    ReaderSerializer, 
    BookOnHandSerializer,
    ReaderWithBooksSerializer,
    ReaderWithBookOnHisHandSerializer
)
from .filters import BookFilter


class BookList(generics.ListAPIView):
    """
    Просмотр книг с фильтрацией по году, названию, автору
    """
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = BookFilter
 

class ReaderBookTotalList(viewsets.ModelViewSet):
    """
    Просмотр читателей с количеством книг на руках
    """
    serializer_class = ReaderWithBooksSerializer
    queryset = Reader.objects.all()
    

class ReaderDetail(generics.RetrieveAPIView):
    """
    Просмотр определенных читателей с книгами, которые у них в данный момент на руках
    """
    queryset = Reader.objects.all()
    serializer_class = ReaderWithBookOnHisHandSerializer



class AuthorList(generics.ListCreateAPIView):
    """
    Просмотр и создание записей об авторах
    """
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    

class DepartmentList(viewsets.ModelViewSet):
    """
    Просмотр и создание записей об отделах
    """
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    

class BookList(viewsets.ModelViewSet):
    """
    Просмотр и создание записей о книгах
    """
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    

class ReaderList(viewsets.ModelViewSet):
    """
    Просмотр и создание записей о читателях/посетителях
    """ 
    queryset = Reader.objects.all()
    serializer_class = ReaderSerializer
    

class BookOnHandList(generics.ListCreateAPIView):
    """
    Просмотр связанной модели (для тестового режима)
    """
    queryset = BookOnHand.objects.all()
    serializer_class = BookOnHandSerializer
    

class TakeBookAPIView(APIView):
    """
    Можно взять книгу. Если книги закончились, то в таблице они станут помечены как недоступные к выдаче.
    """
    
    def post(self, request, id, *args, **kwargs):
        try:
            reader = Reader.objects.get(pk=id)
        except Reader.DoesNotExist:
            return Response({"detail": "Читатель не найден"}, status=status.HTTP_404_NOT_FOUND)

        book_id = request.data.get('book_id')

        try:
            book = Book.objects.get(pk=book_id)
        except Book.DoesNotExist:
            return Response({"detail": "Книга не найдена"}, status=status.HTTP_404_NOT_FOUND)

        if book.is_available and not BookOnHand.objects.filter(book=book, reader=reader, is_returned=False).exists():
            BookOnHand.objects.create(book=book, reader=reader)
            available_books = BookOnHand.objects.filter(book=book, is_returned=False).count()
            if available_books == 0:
                book.is_available = False
            return Response({"detail": "Книга выдана"}, status=status.HTTP_200_OK)
        else:
            return Response({"detail": "Книги в данный момент не доступны"}, status=status.HTTP_400_BAD_REQUEST)
        

class ReturnBookAPIView(APIView):
    """
    Можно вернуть книгу. Если ваш экземпляр был последний (или единственный),
    то книги в таблице будут помечены как отныне доступные.
    """
    
    def post(self, request, id, *args, **kwargs):

        try:
            reader = Reader.objects.get(pk=id)
            book_id = request.data.get('book_id')
            book = Book.objects.get(pk=book_id)
            book_on_hand = BookOnHand.objects.get(reader=reader, book_id=book_id, is_returned=False)
        except (Reader.DoesNotExist, BookOnHand.DoesNotExist, Book.DoesNotExist):
            return Response({"detail": "Запись о долге не найдена"}, status=status.HTTP_404_NOT_FOUND)
        
        if book_on_hand.is_returned:
            return Response({"detail": "Книга уже возвращена"}, status=status.HTTP_400_BAD_REQUEST)


        book_on_hand.is_returned = True
        book_on_hand.save()
        
        if book.is_available == False:
            book.is_available = True
            book.save()

        return Response({"detail": "Книга возвращена"}, status=status.HTTP_200_OK)
