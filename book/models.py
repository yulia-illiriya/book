from django.db import models
from datetime import timezone

# - Уметь хранить информацию о книгах, отделах и посетителях.
#     + У книги есть название, автор и год издания и количество экземпляров.
#     + У отдела есть название.
#     + У посетителя есть имя.
#     + Каждая книга принадлежит какому-то отделу.
#     + Экземпляры книг посетители могут брать на руки.

class Author(models.Model):
    fullname = models.CharField("ФИО", unique=True, max_length=255)
    
    def __str__(self):
        return self.fullname
    
    
class Department(models.Model):
    department_name = models.CharField("Отдел", unique=True, max_length=255)
    
    def __str__(self):
        return self.department_name
    

class Book(models.Model):
    name = models.CharField("Книга", max_length=255)
    author = models.ForeignKey(Author, on_delete=models.CASCADE, verbose_name="Aвтор", related_name="book")
    year_published = models.PositiveIntegerField("Год публикации")
    number_of_books = models.PositiveIntegerField("Количество экземпляров")
    department = models.ForeignKey(Department, on_delete=models.SET_NULL, null=True, blank=True, verbose_name="Отдел")
    is_available = models.BooleanField("В наличии", default=True)
    
    def __str__(self):
        return self.name
    
    
class Reader(models.Model):
    name = models.CharField("Читатель", max_length=255)
    first_visit = models.DateField("Первый визит", auto_now_add=True)
    
    def __str__(self):
        return self.name
    
class BookOnHand(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE, verbose_name="Книга")
    reader = models.ForeignKey(Reader, on_delete=models.CASCADE, verbose_name="Читатель")
    taken_date = models.DateTimeField("Выдана", auto_now_add=True)
    is_returned = models.BooleanField("Возвращена?", default=False)
    return_time = models.DateTimeField("Время возврата", null=True, blank=True, default=None)
    
    def save(self, *args, **kwargs):
        if self.is_returned and not self.return_time:
            self.return_time = timezone.now()
        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.reader.name} borrowed {self.book.name}"