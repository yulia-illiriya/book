from django.urls import path
from rest_framework.routers import DefaultRouter
from book.views import (
    AuthorList, 
    DepartmentList, 
    BookList, 
    ReaderList, 
    BookOnHandList, 
    ReaderBookTotalList, 
    ReaderDetail,
    TakeBookAPIView,
    ReturnBookAPIView
    )

router = DefaultRouter()
router.register(r'readers', ReaderList, basename='reader-list')
router.register(r'book', BookList, basename='book-list')
router.register(r'department', DepartmentList, basename='department')


urlpatterns = [
    path('authors/', AuthorList.as_view(), name='author-list'),
    path('departments/', DepartmentList.as_view({'create': 'post', 'get': 'list', 'destroy': 'delete', 'update': 'put'}), name='department-list'),
    path('books/', BookList.as_view({'get': 'list', 'destroy': 'delete', 'update': 'put'}), name='book-list'),
    path('readers/', ReaderList.as_view({'get': 'list'}), name='reader-list'),
    path('booksonhand/', BookOnHandList.as_view(), name='bookonhand-list'),
    path('reader-with-total-books/', ReaderBookTotalList.as_view({'get': 'list'}), name='book-total'),
    path('readers/<int:id>/', ReaderDetail.as_view(), name='reader-info'),
    path('readers/<int:id>/take-book/', TakeBookAPIView.as_view(), name='take-book'),
    path('readers/<int:id>/return-book/', ReturnBookAPIView.as_view(), name='return-book')
    ]

urlpatterns += router.urls