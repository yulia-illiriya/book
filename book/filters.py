import django_filters
from .models import Book

class BookFilter(django_filters.FilterSet):    
    """
        Filters for Book API.

        - `author__fullname`: Exact match for author's full name.
        - `year_published`: Exact match for the year of publication.
        - `department__department_name`: Exact match for department's name.
        - `is_available`: Exact match for book's availability.

    """
    class Meta:
        model = Book
        fields = {
            'author__fullname': ['exact'],
            'year_published': ['exact'],
            'department__department_name': ['exact'],
            'is_available': ['exact'],
        }
    