from rest_framework import serializers
from .models import Author, Department, Book, Reader, BookOnHand

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = '__all__'
        

class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = '__all__'
        

class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = [
            'author', 
            'name', 
            'department', 
            'year_published', 
            'number_of_books', 
            'is_available'
            ]
    

class ReaderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reader
        fields = '__all__'
        

class BookOnHandSerializer(serializers.ModelSerializer):
    book = BookSerializer()
    
    class Meta:
        model = BookOnHand
        fields = ['book', 'taken_date']
        
        
class ReaderWithBooksSerializer(serializers.ModelSerializer):
    total_books_on_hand = serializers.SerializerMethodField()

    class Meta:
        model = Reader
        fields = ['id', 'name', 'total_books_on_hand']

    def get_total_books_on_hand(self, obj):
        return obj.bookonhand_set.filter(is_returned=False).count()
    
    
class ReaderWithBookOnHisHandSerializer(serializers.ModelSerializer):
    books = serializers.SerializerMethodField()        
        
    class Meta:
        model = Reader
        fields = ['id', 'name', 'books']
