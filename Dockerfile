FROM python:3.9.13

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /

COPY . /

RUN pip install --no-cache-dir -r requirements.txt

RUN python manage.py migrate && python manage.py makemigrations

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]